name := """dictionary-service"""
organization := "ru.qwex"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.13.7"

//crossScalaVersions := Seq("2.12.10", "2.11.12")

libraryDependencies += guice

val circeVersion = "0.14.1"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % circeVersion)

libraryDependencies += "com.dripower" %% "play-circe" % "2712.0"
// libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.3" % Test
