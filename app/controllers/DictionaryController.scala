package controllers

import actions.DictionaryActionRefiner
import data.storages.DictionaryStorage
import javax.inject.Inject
import models.{DictionaryUpdate, EntryFilter, NewDictionary}
import play.api.mvc.{BaseController, ControllerComponents}
import io.circe.syntax._
import io.circe.generic.auto._
import data.json.DictionaryEntryStorageEncoder.encoder
import utils.CirceWithJsonError

class DictionaryController @Inject()(val controllerComponents: ControllerComponents)
  extends BaseController with CirceWithJsonError
{

  private implicit val executionContext = controllerComponents.executionContext

  def all = Action { implicit request =>
    Ok(DictionaryStorage.list.asJson)
  }

  def add = Action(circe.json[NewDictionary]) {
    implicit request =>
      val newDictionary = request.body
      DictionaryStorage.add(newDictionary).fold(
        error => BadRequest(Map("error" -> error).asJson),
        dictionary => Ok(dictionary.asJson)
      )
  }

  def byId(
    dictionaryId: Int
  ) = Action.andThen(DictionaryActionRefiner(dictionaryId)) {  request =>
    Ok(request.dictionary.asJson)
  }

  def update(
    dictionaryId: Int
  ) = Action(circe.json[DictionaryUpdate]) { request =>
    val dictionaryUpdate = request.body
    val updated = DictionaryStorage.update(dictionaryId, dictionaryUpdate)
    Ok(Map("updated" -> updated).asJson)
  }

  def entries(
    dictionaryId: Int
  ) = Action.andThen(DictionaryActionRefiner(dictionaryId)) {
    implicit request =>
      val dictionary = request.dictionary
      Ok(dictionary.entries.asJson)
  }

  def findEntries(
    dictionaryId: Int,
    maybeEntryName: Option[String],
    maybeEntryId: Option[Int],
    maybeCreatedUTSFrom: Option[Long],
    maybeCreatedUTSTo: Option[Long],
    maybeModifiedUTSFrom: Option[Long],
    maybeModifiedUTSTo: Option[Long]
  ) = Action.andThen(DictionaryActionRefiner(dictionaryId)) {
    implicit request =>
      val dictionary = request.dictionary
      val entryFilter = EntryFilter(
        maybeEntryName: Option[String],
        maybeEntryId: Option[Int],
        maybeCreatedUTSFrom: Option[Long],
        maybeCreatedUTSTo: Option[Long],
        maybeModifiedUTSFrom: Option[Long],
        maybeModifiedUTSTo: Option[Long]
      )
      Ok(dictionary.entries.list(entryFilter).asJson)
  }

  def deleteById(
    dictionaryId: Int
  ) = Action { request =>
    val deleted = DictionaryStorage.deleteById(dictionaryId)
    Ok(
      Map("deleted" -> deleted).asJson
    )
  }

}
