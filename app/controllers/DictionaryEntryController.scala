package controllers

import actions.{DictionaryActionRefiner, DictionaryEntryActionRefiner}
import javax.inject.Inject
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}
import io.circe.syntax._
import io.circe.generic.auto._
import models.{DictionaryEntryUpdate, NewDictionaryEntry}
import utils.CirceWithJsonError
class DictionaryEntryController @Inject()(val controllerComponents: ControllerComponents)
  extends BaseController with CirceWithJsonError
{
  private implicit val excecutionContext = controllerComponents.executionContext

  def byId(
    dictionaryId: Int,
    entryId: Int
  ): Action[AnyContent] =
    Action
      .andThen(DictionaryActionRefiner(dictionaryId))
      .andThen(DictionaryEntryActionRefiner(entryId)) {
        request =>
          val entry = request.entry
          Ok(entry.asJson)
      }

  def deleteByIds(
    dictionaryId: Int
  ): Action[Seq[Int]] =
    Action(circe.json[Seq[Int]]).andThen(DictionaryActionRefiner(dictionaryId)) {
      request =>
        val entryIds = request.body
        val dictionary = request.dictionary
        val entries = dictionary.entries

        val deleted = {
          for {
            id <- entryIds
          } yield entries.deleteById(id)
        }.sum
        Ok(Map("deleted" -> deleted).asJson)
    }

  def deleteById(
    dictionaryId: Int,
    entryId: Int
  ): Action[AnyContent] =
    Action.andThen(DictionaryActionRefiner(dictionaryId)) {
      request => {
        val dictionary = request.dictionary
        val deleted = dictionary.entries.deleteById(entryId)
        Ok(Map("deleted" -> deleted).asJson)
      }
    }

  def add(
    dictionaryId: Int
  ): Action[NewDictionaryEntry] =
    Action(circe.json[NewDictionaryEntry]).andThen(DictionaryActionRefiner(dictionaryId)) {
      request =>
        val newEntry = request.body
        val dictionary = request.dictionary
        dictionary.entries.add(newEntry).fold(
          error => BadRequest(Map("error" -> error).asJson),
          dictionary => Ok(dictionary.asJson)
        )
    }

  def update(
    dictionaryId: Int,
    entryId: Int
  ) =
    Action(circe.json[DictionaryEntryUpdate]).andThen(DictionaryActionRefiner(dictionaryId))  {
      request =>
        val entryUpdate = request.body
        val dictionary = request.dictionary
        val updated = dictionary.entries.update(entryId, entryUpdate)
        Ok(Map("updated" -> updated).asJson)
    }

}
