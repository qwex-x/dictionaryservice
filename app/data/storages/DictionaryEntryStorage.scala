package data.storages

import models.{DictionaryEntry, DictionaryEntryUpdate, EntryFilter, NewDictionaryEntry}

class DictionaryEntryStorage(
  entries: collection.mutable.ListBuffer[DictionaryEntry]
) {

  def byId(entryId: Int): Option[DictionaryEntry] = {
    entries.find(_.id == entryId)
  }

  def list = entries.toSeq
  def list(entryFilter: EntryFilter) =
    entries.toSeq.filter(entryFilter(_))

  def deleteById(entryId: Int):Int = {
    val entryIndex =
      entries.indexWhere(_.id == entryId, 0)
    if (entryIndex >= 0)
      entries.remove(entryIndex)
    if (entryIndex >= 0 ) 1 else 0
  }

  def add(
    newEntry: NewDictionaryEntry
  ): Either[String, DictionaryEntry] = {
    val maybeId = newEntry.id

    maybeId.map( id =>
      entries.find(_.id == id).map(
        entry => s"Dictionary entry with id = ${entry.id} already exists!"
      ).toLeft(id)
    ).getOrElse(
      Right (if (entries.nonEmpty) entries.map(_.id).max + 1 else 1)
    ).map {
      id =>
        val entry = newEntry.create(id)
        entries.addOne(entry)
        entry
    }
  }

  def update(
    entryId: Int,
    entryUpdate: DictionaryEntryUpdate
  ):Int = {
    for {
      (entry, index) <- entries.view.zipWithIndex.find(_._1.id == entryId)
    } yield {
      entries.update(index, entryUpdate.update(entry))
    }
  }.map(_ => 1).getOrElse(0)

}

object DictionaryEntryStorage {
  def empty =
    new DictionaryEntryStorage(collection.mutable.ListBuffer.empty)
  def apply(entries: DictionaryEntry*) = {
    new DictionaryEntryStorage(collection.mutable.ListBuffer(entries: _*))
  }
}
