package data.storages

import models.{Dictionary, DictionaryUpdate, NewDictionary}
import play.i18n.Messages

object DictionaryStorage {

  private val dictionaries = {
    import models.Dictionaries._
    collection.mutable.ListBuffer(
      empty, ellochka, chatlanoPatsaksky
    )
  }

  def byId(dictionaryId: Int): Option[Dictionary] = {
    dictionaries.find(_.id == dictionaryId)
  }

  def add(
    newEntry: NewDictionary
  ): Either[String, Dictionary] = {
    val maybeId = newEntry.id

    maybeId.map( id =>
      dictionaries.find(_.id == id).map(
        dictionary =>  s"Dictionary with id = ${dictionary.id} already exists!"
      ).toLeft(id)
    ).getOrElse(
      Right(if (dictionaries.nonEmpty) dictionaries.map(_.id).max + 1 else 1)
    ).map {
      id =>
        val entry = newEntry.create(id)
        dictionaries.addOne(entry)
        entry
    }
  }

  def list = dictionaries.toSeq

  def deleteById(dictionaryId: Int):Int = {
    val dictionaryIndex =
      dictionaries.indexWhere(_.id == dictionaryId, 0)
    if (dictionaryIndex >= 0)
      dictionaries.remove(dictionaryIndex)
    if (dictionaryIndex >= 0 ) 1 else 0
  }

  def update(
    dictionaryId: Int,
    dictionaryUpdate: DictionaryUpdate
  ):Int = {
    {
      for {
        (dictionary, index) <- dictionaries.view.zipWithIndex.find(_._1.id == dictionaryId)
      } yield {
        dictionaries.update(index, dictionaryUpdate.update(dictionary))
      }
    }.map(_ => 1).getOrElse(0)
  }

}
