package data.json

import data.storages.DictionaryEntryStorage
import io.circe.Encoder
import io.circe.syntax._
import io.circe.generic.auto._

object DictionaryEntryStorageEncoder {

  implicit val encoder: Encoder[DictionaryEntryStorage] = (storage: DictionaryEntryStorage) => storage.list.asJson

}
