package models

import java.time.Instant
import io.circe.Json

case class DictionaryEntry(
  id: Int,
  name: String,
  created: Instant,
  modified: Instant,
  data: Json
)

object DictionaryEntry {
  def apply(
    id: Int,
    name: String,
    data: Json,
    created: Instant
  ): DictionaryEntry = {
    DictionaryEntry(
      id = id,
      name = name,
      created = created,
      modified =  created,
      data = data
    )
  }
}
