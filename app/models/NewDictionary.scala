package models

import java.time.Instant

import io.circe.Json

case class NewDictionary(
  id: Option[Int],
  name: String,
  properties: Option[Json]
) {
  def create(
    newId: Int
  ): Dictionary = {
    val created = Instant.now()
    Dictionary(
      id = newId,
      name = name,
      created = created,
      modified = created,
      properties = properties.getOrElse(Json.obj())
    )
  }
}
