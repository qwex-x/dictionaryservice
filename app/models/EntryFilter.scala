package models

import java.time.Instant

import utils.Utils

class EntryFilter private[EntryFilter](filters: List[DictionaryEntry => Boolean]) {
  def apply(entry: DictionaryEntry): Boolean =
    filters.isEmpty || filters.forall(_(entry))
}

object EntryFilter {

  private def entryFilter[A](filter: (DictionaryEntry,  A) => Boolean) =
    Utils.swap(filter)

  private def unixTimeStampToInstant(epochSecond: Long) =
    Instant.ofEpochSecond(epochSecond)

  def apply(
    maybeEntryName: Option[String],
    maybeEntryId: Option[Int],
    maybeCreatedUTSFrom: Option[Long],
    maybeCreatedUTSTo: Option[Long],
    maybeModifiedUTSFrom: Option[Long],
    maybeModifiedUTSTo: Option[Long]
  ): EntryFilter =
    new EntryFilter(
      List(
        maybeEntryName.map(
          entryFilter(_.name == _)
        ),
        maybeEntryId.map(
          entryFilter(_.id == _)
        ),
        maybeCreatedUTSFrom.
          map(_ - 1).
          map(unixTimeStampToInstant).
          map(entryFilter(_.created.isAfter(_))),
        maybeCreatedUTSTo.
          map(_ + 1).
          map(unixTimeStampToInstant).
          map(entryFilter(_.created.isBefore(_))),
        maybeModifiedUTSFrom.
          map(_ - 1).
          map(unixTimeStampToInstant).
          map(entryFilter(_.created.isAfter(_))),
        maybeModifiedUTSTo.
          map(_ + 1).
          map(unixTimeStampToInstant).
          map(entryFilter(_.created.isBefore(_)))
      ).flatten
    )

}
