package models

import java.time.Instant

import io.circe.Json

case class DictionaryUpdate(
  name: Option[String],
  properties: Option[Json]
) {
  def update(dictionary: Dictionary): Dictionary =
    dictionary.copy(
      name = name.getOrElse(dictionary.name),
      properties = properties.getOrElse(dictionary.properties),
      modified = Instant.now()
    )
}
