package models

import java.time.Instant

import io.circe.Json

case class DictionaryEntryUpdate(
  name: Option[String],
  data: Option[Json]
) {
  def update(entry: DictionaryEntry): DictionaryEntry = {
    entry.copy(
      name = name.getOrElse(entry.name),
      data = data.getOrElse(entry.data),
      modified = Instant.now()
    )
  }
}
