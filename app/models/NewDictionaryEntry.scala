package models

import java.time.Instant

import io.circe.Json

case class NewDictionaryEntry(
  id: Option[Int],
  name: String,
  data: Option[Json]
) {
  def create(
    newId: Int
  ): DictionaryEntry = {
    val created = Instant.now()
    DictionaryEntry(
      id = newId,
      name = name,
      data = data.getOrElse(Json.obj()),
      created = created
    )
  }
}
