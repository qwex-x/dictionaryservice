package models

import java.time.Instant

import data.storages.DictionaryEntryStorage
import io.circe.Json

case class Dictionary(
  id: Int,
  name: String,
  created: Instant,
  modified: Instant,
  properties: Json,
  entries: DictionaryEntryStorage = DictionaryEntryStorage.empty
)


object Dictionaries {
  import io.circe.syntax._

   val created = Instant.ofEpochSecond(1639401155)//Instant.now()

  val  ellochka = Dictionary(
    id = 1,
    name = "Словарь Эллочки",
    created = created,
    modified = created,
    properties = Json.obj(),
    entries = DictionaryEntryStorage(
      DictionaryEntry(
        id = 1,
        name = "Хамите.",
        data = Json.obj(),
        created = created
      ),
      DictionaryEntry(
        id = 2,
        name = "Хо-хо!",
        data = Map(
          "explanation" -> "Выражает, в зависимости от обстоятельств, иронию, удивление, восторг, ненависть, радость, презрение и удовлетворенность"
        ).asJson,
        created = created
      ),
      DictionaryEntry(
        id = 3,
        name = "Знаменито.",
        data = Json.obj(),
        created = created
      ),
      DictionaryEntry(
        id = 4,
        name = "Мрачный.",
        data = Map(
          "explanation" -> "По отношению ко всему. Например: «Мрачный Петя пришёл», «Мрачная погода», «Мрачный случай», «Мрачный кот» и т. д."
        ).asJson,
        created = created
      ),
      DictionaryEntry(
        id = 5,
        name = "Мрак.",
        data = Json.obj(),
        created = created
      ),
      DictionaryEntry(
        id = 6,
        name = "Жуть.",
        data = Map(
          "explanation" -> "Жуткий. Например, при встрече с доброй знакомой: «жуткая встреча»"
        ).asJson,
        created = created
      ),
      DictionaryEntry(
        id = 7,
        name = "Парниша.",
        data = Map(
          "explanation" -> "По отношению ко всем знакомым мужчинам, независимо от возраста и общественного положения"
        ).asJson,
        created = created
      ),
      DictionaryEntry(
        id = 8,
        name = "Не учите меня жить..",
        data = Json.obj(),
        created = created
      ),
      DictionaryEntry(
        id = 9,
        name = "Как ребёнка.",
        data = Map(
          "explanation" -> "«Я его бью, как ребёнка» — при игре в карты. «Я его срезала, как ребёнка» — как видно, в разговоре с ответственным съёмщиком)"
        ).asJson,
        created = created
      ),
      DictionaryEntry(
        id = 10,
        name = "Кр-р-расота!",
        data = Json.obj(),
        created = created
      ),
      DictionaryEntry(
        id = 11,
        name = "Толстый и красивы.",
        data = Map(
          "explanation" -> "Употребляется как характеристика неодушевлённых и одушевлённых предметов"
        ).asJson,
        created = created
      ),
      DictionaryEntry(
        id = 12,
        name = "Поедем на извозчике.",
        data = Map(
          "explanation" -> "Говорится мужу"
        ).asJson,
        created = created
      ),
      DictionaryEntry(
        id = 13,
        name = "Поедем на таксо.",
        data = Map(
          "explanation" -> "Знакомым мужского пола"
        ).asJson,
        created = created
      ),
      DictionaryEntry(
        id = 14,
        name = "У вас вся спина белая",
        data = Map(
          "explanation" -> "шутка"
        ).asJson,
        created = created
      ),
      DictionaryEntry(
        id = 15,
        name = "Подумаешь",
        data = Json.obj(),
        created = created
      ),
      DictionaryEntry(
        id = 16,
        name = "Уля.",
        data = Map(
          "explanation" -> "Ласкательное окончание имен. Например: Мишуля, Зинуля"
        ).asJson,
        created = created
      ),
      DictionaryEntry(
        id = 17,
        name = "Ого!",
        data = Map(
          "explanation" -> "Ирония, удивление, восторг, ненависть, радость, презрение и удовлетворённость"
        ).asJson,
        created = created
      ),
    )
  )

  val chatlanoPatsaksky =
    Dictionary(
      id = 3,
      name = "Краткий чатлано-пацакский словарь",
      created = created,
      modified = created,
      properties = Json.obj(
        "originState" -> "Планета Плюк".asJson,
        "knownNativeSpeakers" -> List("Би", "Уэф", "Господин ПЖ").asJson,
      ).asJson,
      entries = DictionaryEntryStorage(
        DictionaryEntry(
          id = 1,
          name = "КЦ",
          data = Map(
            "meaning" -> "спичка"
          ).asJson,
          created = created
        ),
        DictionaryEntry(
          id = 2,
          name = "ЦАК",
          data = Map(
            "meaning" -> "колокольчик для носа"
          ).asJson,
          created = created
        ),
        DictionaryEntry(
          id = 3,
          name = "ЭЦИХ",
          data = Map(
            "meaning" -> "колокольчик для носа"
          ).asJson,
          created = created
        ),
        DictionaryEntry(
          id = 4,
          name = "ЭЦИЛОПП",
          data = Map(
            "meaning" -> "представитель власти"
          ).asJson,
          created = created
        ),
        DictionaryEntry(
          id = 5,
          name = "ПЕЕЛАЦ",
          data = Map(
            "meaning" -> "межзвёздный корабль"
          ).asJson,
          created = created
        ),
        DictionaryEntry(
          id = 6,
          name = "ГРАВИЦАППА ",
          data = Map(
            "meaning" -> "деталь от мотора пепелаца"
          ).asJson,
          created = created
        ),
        DictionaryEntry(
          id = 7,
          name = "КЮ",
          data = Map(
            "meaning" -> " допустимое в обществе ругательство"
          ).asJson,
          created = created
        ),
        DictionaryEntry(
          id = 8,
          name = "Ку",
          data = Map(
            "meaning" -> "все остальные слова"
          ).asJson,
          created = created
        ),
      ),
    )

  def empty = Dictionary(
    id = 4,
    name = "Пустой словарь",
    created = created,
    modified = created,
    properties = Json.obj(),
    entries = DictionaryEntryStorage.empty
  )
}
