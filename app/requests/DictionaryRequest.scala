package requests


import models.Dictionary
import play.api.mvc.{Request, WrappedRequest}

class DictionaryRequest[A](val dictionary: Dictionary, request: Request[A]) extends WrappedRequest(request)
