package requests

import models.DictionaryEntry
import play.api.mvc.WrappedRequest

class DictionaryEntryRequest[A](val entry: DictionaryEntry, request: DictionaryRequest[A]) extends WrappedRequest(request){
  val dictionary = request.dictionary
}
