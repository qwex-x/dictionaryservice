package actions

import data.storages.DictionaryStorage
import play.api.mvc.{ActionRefiner, Request, Result, Results}
import requests.DictionaryRequest
import io.circe.syntax._
import play.api.http.Writeable

import scala.concurrent.{ExecutionContext, Future}

case class  DictionaryActionRefiner(dictionaryId: Int) (
  implicit
  val executionContext: ExecutionContext,
  val wrt: Writeable[io.circe.Json]
)  extends ActionRefiner[Request, DictionaryRequest] {
  def refine[A](input: Request[A]): Future[Either[Result, DictionaryRequest[A]]] = Future.successful{
    DictionaryStorage.byId(dictionaryId).toRight(
      Results.NotFound(Map("error" ->  s"Dictionary with id = ${dictionaryId} not exists").asJson)
    ).map(
      dictionary => new DictionaryRequest[A](dictionary, input)
    )
  }
}
