package actions

import io.circe.syntax._
import play.api.http.Writeable
import play.api.mvc.{ActionRefiner, Result, Results}
import requests.{DictionaryEntryRequest, DictionaryRequest}

import scala.concurrent.{ExecutionContext, Future}

case class  DictionaryEntryActionRefiner(entryId: Int) (
  implicit
  val executionContext: ExecutionContext,
  val wrt: Writeable[io.circe.Json]
)  extends ActionRefiner[DictionaryRequest, DictionaryEntryRequest] {
  def refine[A](input: DictionaryRequest[A]): Future[Either[Result, DictionaryEntryRequest[A]]] = Future.successful{
    input.dictionary.entries.byId(entryId).toRight(
      Results.NotFound(Map("error" ->  s"Entry with id = ${entryId} not exists").asJson)
    ).map(
      entry => new DictionaryEntryRequest[A](entry, input)
    )
  }
}
