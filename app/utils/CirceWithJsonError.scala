package utils

import play.api.mvc.{Result, Results}
import io.circe.syntax._
import play.api.libs.circe.Circe

trait CirceWithJsonError extends Circe {

  override protected def onCirceError(e: io.circe.Error):Result = {
    Results.BadRequest(Map("error" -> e.toString).asJson)
  }

}
