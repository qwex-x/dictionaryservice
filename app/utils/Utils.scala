package utils

object Utils {

  def swap[A,B,C](f: (A,B)=>C) =
    (x:B) => (y:A) => f(y,x)

}
