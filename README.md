# API

## Получение списка словарей

`GET /dictionaries`
### Запрос
```javascript
fetch('/dictionaries', {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  }
});
```
### Результат
`Status Code: 200 OK`
```json
[{"id":4,"name":"Пустой словарь","created":"2021-12-13T13:12:35Z","modified":"2021-12-13T13:12:35Z","properties":{},"entries":[]}...]
```

## Получение словаря по идентификатору

`GET     /dictionaries/:dictionaryId`
### Запрос
```javascript
fetch('/dictionaries/4', {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  }
});
```
### Результат
`Status Code: 200 OK`
```json
[{"id":4,"name":"Пустой словарь","created":"2021-12-13T13:12:35Z","modified":"2021-12-13T13:12:35Z","properties":{},"entries":[]}...]
```

## Получение несуществующего словаря

`GET     /dictionaries/:dictionaryId`
### Запрос
```javascript
fetch('/dictionaries/2', {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  }
});
```
### Результат
`Status Code: 404 Not Found`
```json
{"error":"Dictionary with id = 2 not exists"}
```

## Получение словаря с нечисловым идентификатором

`GET     /dictionaries/:dictionaryId`
### Запрос
```javascript
fetch('/dictionaries/oprst', {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  }
});
```
### Результат
`Status Code: 400 Bad Request`
```json
{"error":{"requestId":6,"message":"Cannot parse parameter dictionaryId as Int: For input string: \"oprst\""}}
```

# Добавление нового словаря

`POST    /dictionaries`
### Запрос
```javascript
fetch('/dictionaries', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  body: JSON.stringify({
    "id": 15, "name" : "Бестолковый словарь", 
    "properties" : {"description" : "Ничего дельного. Он вам не поможет." }
  })
})
```
#### Формат body

```json
{"id" : "идентификатор", "name" : "имя словаря", "properties" : "свойства словаря"}
```
Поля `name` и `properties` - опциональные.

#### Результат
`Status Code: 200 OK`
```json
{
  "id":15,
  "name":"Бестолковый словарь",
  "created":"2021-12-13T23:07:24.133Z",
  "modified":"2021-12-13T23:07:24.133Z",
  "properties":{"description":"Ничего дельного. Он вам не поможет."},
  "entries":[]
}
```

# Добавление нового словаря (неправильный body)

`POST    /dictionaries`
### Запрос
```javascript
fetch('/dictionaries', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  body: JSON.stringify({
    "id": "пятнадцать", "name" : "Бестолковый словарь", 
    "properties" : {"description" : "Ничего дельного. Он вам не поможет." }
  })
})
```
#### Формат body

```json
{"id" : "идентификатор", "name" : "имя словаря", "properties" : "свойства словаря"}
```
Поля `name` и `properties` - опциональные.

#### Результат
`Status Code: 400 Bad Request`
```json
{"error":"DecodingFailure(Int, List(DownField(id)))"}
```

# Удаление словаря

`DELETE   /dictionaries/:dcitionaryId`

### Запрос
```javascript
fetch('/dictionaries/1', {
  method: 'DELETE',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  }
})
```

#### Результат
`Status Code: 200 OK`
```json
{"deleted":1}
```

# Удаление несуществующего словаря

`DELETE   /dictionaries/:dcitionaryId`

### Запрос
```javascript
fetch('/dictionaries/100500', {
  method: 'DELETE',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  }
})
```

#### Результат
`Status Code: 200 OK`
```json
{"deleted":0}
```

# Добавление нового словаря с id существующего словаря

`POST    /dictionaries`
### Запрос
```javascript
fetch('/dictionaries', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  body: JSON.stringify({
    "id": 3, "name" : "Словарь повторов", 
    "properties" : {"repeat" : "loop" }
  })
})
```

#### Результат
`Status Code: 400 Bad Request`
```json
{"error":"Dictionary with id = 3 already exists!"}
```

# Добавление нового словаря без указания id

`POST    /dictionaries`
### Запрос
```javascript
fetch('/dictionaries', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  body: JSON.stringify({
    "name" : "Какой-то словарь"
  })
})
```

#### Результат
`Status Code: 200 OK`
```json
{
  "id":5,"name":"Какой-то словарь",
  "created":"2021-12-16T21:00:15.927Z",
  "modified":"2021-12-16T21:00:15.927Z",
  "properties":{},
  "entries":[]
}
```

# Обновление названия и/или свойств словаря

`POST    /dictionaries/:dictionaryId`
### Запрос
```javascript
fetch('/dictionaries/4', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  body: JSON.stringify({"name" : "Словарь", "properties"s: {}})
})
```
#### Формат body

```json
{"name" : "имя словаря", "properties" : "свойства словаря"}
```
Оба поля `name` и `properties` - опциональные.
#### Результат
`Status Code: 200 OK`
```json
{"updated":1}
```

# Получение списка элементов конретного словаря

`GET /dictionaries/:dictionaryId/entries`
### Запрос
```javascript
fetch('/dictionaries/3/entries', {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  }
});
```
### Результат
`Status Code: 200 OK`
```json
[{"id":1,"name":"КЦ","created":"2021-12-13T13:12:35Z","modified":"2021-12-13T13:12:35Z","data":{"meaning":"спичка"}},{"id":2,"name":"ЦАК","created":"2021-12-13T13:12:35Z","modified":"2021-12-13T13:12:35Z","data":{"meaning":"колокольчик для носа"}},{"id":3,"name":"ЭЦИХ","created":"2021-12-13T13:12:35Z","modified":"2021-12-13T13:12:35Z","data":{"meaning":"колокольчик для носа"}},{"id":4,"name":"ЭЦИЛОПП","created":"2021-12-13T13:12:35Z","modified":"2021-12-13T13:12:35Z","data":{"meaning":"представитель власти"}},{"id":5,"name":"ПЕЕЛАЦ","created":"2021-12-13T13:12:35Z","modified":"2021-12-13T13:12:35Z","data":{"meaning":"межзвёздный корабль"}},{"id":6,"name":"ГРАВИЦАППА ","created":"2021-12-13T13:12:35Z","modified":"2021-12-13T13:12:35Z","data":{"meaning":"деталь от мотора пепелаца"}},{"id":7,"name":"КЮ","created":"2021-12-13T13:12:35Z","modified":"2021-12-13T13:12:35Z","data":{"meaning":" допустимое в обществе ругательство"}},{"id":8,"name":"Ку","created":"2021-12-13T13:12:35Z","modified":"2021-12-13T13:12:35Z","data":{"meaning":"все остальные слова"}}]
```

# Получение списка элементов несуществующего словаря 

`GET /dictionaries/:dictionaryId/entries`
### Запрос
```javascript
fetch('/dictionaries/20/entries', {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  }
});
```
### Результат
`Status Code: 404 Not Found`
```json
{"error":"Dictionary with id = 20 not exists"}
```

# Поиск элементов контретного словаря

`GET /dictionaries/:dictionaryId/entries/search?name=...&id=...&createdFrom=...&createdTo=...&modifiedFrom...&modifiedTo=...`

Парметры поиска:

`name` - имя элемента словаря. `id` - идентификатор словаря. `createdFrom` и `createdTo` задают верхнюю и нижнию границу диапазона даты создания элемента словаря. `modifiedFrom` и `modifiedTo` задают верхнюю и нижнию границу диапазона даты модификации словаря. 

`createdFrom`, `createdTo`, `modifiedFrom`, `modifiedTo` задаются числом unix timestamp

### Запрос
```
fetch('/dictionaries/3/entries/search?name=КЦ', {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  }
});
```

Все параметры поиска являются опциональными.

### Результат
`Status Code: 200 OK`
```json
[{"id":1,"name":"КЦ","created":"2021-12-13T13:12:35Z","modified":"2021-12-13T13:12:35Z","data":{"meaning":"спичка"}}]
```

## Получение элемента словаря по идентификатору

`GET  /dictionaries/:dictionaryId/entries/3`
### Запрос
```javascript
fetch('/dictionaries/3/entries/3', {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  }
});
```
### Результат
`Status Code: 200 OK`
```json
{"id":3,"name":"ЭЦИХ","created":"2021-12-13T13:12:35Z","modified":"2021-12-13T13:12:35Z","data":{"meaning":"колокольчик для носа"}}
```

## Получение несуществующего элемента словаря

`GET  /dictionaries/:dictionaryId/entries/:entityId`
### Запрос
```javascript
fetch('/dictionaries/3/entries/-1', {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  }
});
```
### Результат
`Status Code: 404 Not Found`
```json
{"error":"Entry with id = -1 not exists"}
```

## Получение элемента несуществующего словаря

`GET  /dictionaries/:dictionaryId/entries/:entityId`
### Запрос
```javascript
fetch('/dictionaries/-1/entries/3', {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  }
});
```
### Результат
`Status Code: 404 Not Found`
```json
{"error":"Dictionary with id = -1 not exists"}
```

# Обновление названия и/или данных элемента словаря

`POST    /dictionaries/:dictionaryId/entries/:entryId`
### Запрос
```javascript
fetch('/dictionaries/3/entries/1', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  body: JSON.stringify(
    {
      "name" : "Что значит имя? Роза пахнет розой, Хоть розой назови ее, хоть нет.", 
      "data": {"author" : "У. Шекспир"}
    }
  )
})
```
#### Формат body

```json
{"name" : "новое имя", "data" :  "новые данные"}
```
Оба поля `name` и `properties` - опциональные.
#### Результат
`Status Code: 200 OK`
```json
{"updated":1}
```

# Обновление несуществующего элемента словаря

`POST    /dictionaries/:dictionaryId/entries/:entryId`
### Запрос
```javascript
fetch('/dictionaries/4/entries/1', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  body: JSON.stringify(
    {
      "name" : "Что значит имя? Роза пахнет розой, Хоть розой назови ее, хоть нет.", 
      "data": {"author" : "У. Шекспир"}
    }
  )
})
```

Оба поля `name` и `properties` - опциональные.
#### Результат
`Status Code: 200 OK`
```json
{"updated":0}
```

# Обновление элемента несуществующего словаря

`POST    /dictionaries/:dictionaryId/entries/:entryId`
### Запрос
```javascript
fetch('/dictionaries/-1/entries/1', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  body: JSON.stringify(
    {
      "name" : "Что значит имя? Роза пахнет розой, Хоть розой назови ее, хоть нет.", 
      "data": {"author" : "У. Шекспир"}
    }
  )
})
```

#### Результат
`Status Code: 404 Not Found`
```json
{"error":"Dictionary with id = -1 not exists"}
```

# Удаление элемента словаря

`DELETE   /dictionaries/:dcitionaryId/entries/:entryId`

### Запрос
```javascript
fetch('/dictionaries/3/entries/5', {
  method: 'DELETE',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  }
})
```

#### Результат
`Status Code: 200 OK`
```json
{"deleted":1}
```

# Удаление несуществующего элемента словаря

`DELETE   /dictionaries/:dcitionaryId/entries/:entryId`

### Запрос
```javascript
fetch('/dictionaries/4/entries/5', {
  method: 'DELETE',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  }
})
```

#### Результат
`Status Code: 200 OK`
```json
{"deleted":0}
```

# Удаление элемента несуществующего словаря

`DELETE   /dictionaries/:dcitionaryId/entries/:entryId`

### Запрос
```javascript
fetch('/dictionaries/-1/entries/5', {
  method: 'DELETE',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  }
})
```

#### Результат
`Status Code: 404 Not Found`
```json
{"error":"Dictionary with id = -1 not exists"}
```

# Добавление нового элемента словаря

`POST    /dictionaries/:dictionaryId/entries/:entryId`
### Запрос
```javascript
fetch('/dictionaries/4/entries', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  body: JSON.stringify(
    {
      "id" : 22, 
      "name" : "слово", 
      "data" : {"whenWasIt" : "в начале было"} 
    }
  )
})
```
#### Формат body

```json
{"id" : "идентификатор", "name" : "имя элемента", "data" : "данные элемента словаря"}
```
Поля `name` и `data` - опциональные.

#### Результат
`Status Code: 200 OK`
```json
{"id":22,"name":"слово","created":"2021-12-16T22:00:06.243Z","modified":"2021-12-16T22:00:06.243Z","data":{"whenWasIt":"в начале было"}}
```

# Добавление нового элемента словаря в несуществующий словарь

`POST    /dictionaries/:dictionaryId/entries/:entryId`
### Запрос
```javascript
fetch('/dictionaries/-1/entries', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  body: JSON.stringify(
    {
      "id" : 22, 
      "name" : "слово", 
      "data" : {"whenWasIt" : "в начале было"} 
    }
  )
})
```

#### Результат
`Status Code: 404 Not Found`
```json
{"error":"Dictionary with id = -1 not exists"}
```

# Добавление нового элемента словаря с id существующего словаря

`POST    /dictionaries/:dictionaryId/entries/:entryId`
### Запрос
```javascript
fetch('/dictionaries/3/entries', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  body: JSON.stringify(
    {
      "id" : 2, 
      "name" : "слово", 
      "data" : {"whenWasIt" : "в начале было"} 
    }
  )
})
```

#### Результат
`Status Code: 400 Bad Request`
```json
{"error":"Dictionary entry with id = 2 already exists!"}
```
# Добавление нового элемента словаря без указания id

`POST    /dictionaries/:dictionaryId/entries/:entryId`
### Запрос
```javascript
fetch('/dictionaries/3/entries', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  body: JSON.stringify(
    {
      "name" : "слово", 
      "data" : {"whenWasIt" : "в начале было"} 
    }
  )
})
```
#### Результат
`Status Code: 200 OK`
```json
{"id":9,"name":"слово","created":"2021-12-16T22:03:03.209Z","modified":"2021-12-16T22:03:03.209Z","data":{"whenWasIt":"в начале было"}}
```

# Удаление элементов словаря по списку идентификаторов

`DELETE  /dictionaries/:dictionaryId/entries`

### Запрос
```javascript
fetch('/dictionaries/3/entries', {
  method: 'DELETE',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  body: JSON.stringify(
    [4,6,7]
  )
})
```

#### Результат
`Status Code: 200 OK`
```json
{"deleted":3}
```

# Удаление элементов словаря (неправильный body)

`DELETE  /dictionaries/:dictionaryId/entries`

### Запрос
```javascript
fetch('/dictionaries/3/entries', {
  method: 'DELETE',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  body: JSON.stringify(
    [4,6,"abc"]
  )
})
```

#### Результат
`Status Code: 400 Bad Request`
```json
{"error":"DecodingFailure(Int, List(MoveRight, MoveRight, DownArray))"}
```